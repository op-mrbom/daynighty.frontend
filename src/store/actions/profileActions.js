import * as actionTypes from './actionTypes';
import * as requests from '../../api/apiRequests';

export function fetchProfile(userId) {
    return (dispatch) => {
        dispatch(fetchProfileInit());

        requests.performProfileFetch((res) => {
            dispatch(fetchProfileSuccess(res))
        }, (e) => {
            dispatch(fetchProfileError(e))
        }, userId)
    }
};

export function updateProfile(userId, data) {
    return (dispatch) => {

        dispatch(updateProfileInit());

        requests.performProfileUpdate((res) => {
            dispatch(updateProfileSuccess(res))
        }, (e) => {
            dispatch(updateProfileError(e))
        }, userId, data)
    };
}

export function updateProfileSuccess(res) {
    return {
        type: actionTypes.UPDATE_PROFILE_SUCCESS,
        value: res
    }
}

export function updateProfileInit() {
    return {
        type: actionTypes.UPDATE_PROFILE_INIT
    }
}


export function updateProfileError(errors) {
    return {
        type: actionTypes.UPDATE_PROFILE_ERROR,
        value: errors
    }
}

export function fetchProfileError(errors) {
    return {
        type: actionTypes.PROFILE_FETCH_ERROR,
        value: errors
    }
}

export function fetchProfileSuccess(res) {
    return {
        type: actionTypes.PROFILE_FETCH_SUCCESS,
        value: res
    }
}

export function fetchProfileInit() {
    return {
        type: actionTypes.PROFILE_FETCH_INIT
    }
}


export function fetchHistory(userId) {
    return (dispatch) => {
        dispatch(fetchHistoryInit());

        requests.performBookHistoryFetch(userId, (res) => {
            dispatch(fetchHistorySuccess(res));
        }, (e) => {
            dispatch(fetchHistoryError(e));
        })
    }
}

export function fetchHistoryError(errors) {
    return {
        type: actionTypes.FETCH_BOOK_HISTORY_ERROR,
        errors: errors
    }
}

export function fetchHistoryInit() {
    return {
        type: actionTypes.FETCH_BOOK_HISTORY_INIT
    }
}


export function fetchHistorySuccess(res) {
    return {
        type: actionTypes.FETCH_BOOK_HISTORY_SUCCESS,
        value: res
    }
}