import * as actionTypes from './actionTypes';
import * as requests from '../../api/apiRequests';

export function checkIfAvailable(id, dateFrom, dateTo, personsAmount) {
    return (dispatch) => {
        dispatch(checkAvailableInit());

        requests.checkIfPropositionAvailable(id, dateFrom, dateTo, personsAmount, (res) => {
            dispatch(checkAvailableSuccess(res));
        }, (e) => {dispatch(checkAvailableError(e))})
    }
};


export function setCheckoutProposition({ id, name, from, to, personsAmount, price }) {
    return {
        type: actionTypes.SET_PROPOSITION_TO_CHECKOUT,
        value: true
    }
}

export function bookProposition(data) {
    return (dispatch) => {
        dispatch(bookInit());

        requests.performPropositionBook(data, (res) => {
            dispatch(bookSuccess(res))
        }, (e) => {
            dispatch(bookError(e));
        });
    };
}

export function checkAvailableError(errors) {
    return {
        type: actionTypes.CHECK_PROPOSITION_AVAILABILITY_ERROR,
        errors: errors
    }
}

export function checkAvailableInit() {
    return {
        type: actionTypes.CHECK_PROPOSITION_AVAILABILITY_INIT
    }
}


export function checkAvailableSuccess(res) {
    return {
        type: actionTypes.CHECK_PROPOSITION_AVAILABILITY_SUCCESS,
        value: res
    }
}

export function bookError(errors) {
    return {
        type: actionTypes.BOOK_PROPOSITION_ERROR,
        errors: errors
    }
}

export function bookInit() {
    return {
        type: actionTypes.BOOK_PROPOSITION_INIT
    }
}


export function bookSuccess(res) {
    return {
        type: actionTypes.BOOK_PROPOSITION_SUCCESS,
        value: res
    }
}

export function resetCheckout() {
    return {
        type: actionTypes.RESET_PROPOSITION_CHECKOUT
    }
}