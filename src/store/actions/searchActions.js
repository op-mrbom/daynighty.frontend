import * as actionTypes from './actionTypes';
import * as requests from '../../api/apiRequests';
import RequestProcessor from '../../api/requestsProcessor';

const requestProcessor = new RequestProcessor();

export const searchHints = (value) => {
    return (dispatch) => {
        if (!value) {
            dispatch(updateSearchHints([]));
            // dispatch(resetSearchTimeout());
            return;
        }

        const req = () => {
            requests.performCitySearch(value, (res) => {
                dispatch(updateSearchHints(res))
            }, (e) => {

            });
        };

        req();
    }
};

export const searchSubmit = (searchString, history, page = 1, pageSize = 10) => {
    return (dispatch) => {
        // dispatch(updateSearchInput(searchString));
        dispatch(searchSubmitInit());
        history && history.push("/search");
        //later it'll be an Ajax to API to get list of real results

        requests.performItemSearch(searchString, pageSize, page, (res) => {
            dispatch(searchResultsFetched(res.data, res.headers['total-count'], res.headers['current-page']))
        }, (e) => {
        });

    }
};

export const updateSearchInput = (value) => {
    return {
        type: actionTypes.FAST_SEARCH_CHANGE_INPUT,
        value: value
    }
};

export const updateSearchHints = (value) => {
    value = value.map(v => v.name);
    return {
        type: actionTypes.FAST_SEARCH_UPDATE_HINTS,
        value: value
    }
};

export const resetSearchTimeout = () => {
    return {
        type: actionTypes.FAST_SEARCH_RESET_TIMEOUT,
    }
};

export const setSearchTimeout = (value) => {
    return {
        type: actionTypes.FAST_SEARCH_SET_TIMEOUT,
        value: value
    }
};

export const searchResultsFetched = (list, totalCount, page) => {
    return {
        type: actionTypes.SEARCH_RESULTS_FETCHED,
        value: list,
        count: totalCount,
        page: page
    }
};

export const searchSubmitInit = () => {
    return {
        type: actionTypes.SEARCH_SUBMIT_INIT,
    }
};

export const showSearchOnMainScreen = () => {
    return {
        type: actionTypes.SHOW_SEARCH_ON_MAIN_SCREEN,
    }
};

export const showSearchInNavbar = () => {
    return {
        type: actionTypes.SHOW_SEARCH_IN_NAVBAR,
    }
};

