import * as actionTypes from './actionTypes';
import * as requests from '../../api/apiRequests';

export const fetchItem = (id) => {
    return (dispatch) => {

        dispatch(fetchItemInit());
        //ajax
        requests.fetchItem(id, (res) => {
            dispatch(fetchItemSuccess(res));
        }, (e) => {}, {withPropositions: true, withType: true, withLocation: true});

    };
};

export const fetchItemInit = () => {
    return {
        type: actionTypes.ITEM_FETCH_INIT
    }
};

export const fetchItemSuccess = (data) => {
    return {
        type: actionTypes.ITEM_FETCH_SUCCESS,
        value: data,
    }
};
