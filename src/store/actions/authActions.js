import * as actionTypes from './actionTypes';
import * as requests from '../../api/apiRequests';

function b64ToUtf8(str) {
    return decodeURIComponent(escape(window.atob(str)));
}
const authenticate = (data, dispatch) => {
    dispatch(authInit());
    requests.performAuthenticate((res) => {
        dispatch(authSuccess(res));
    }, (e) => {
        dispatch(authError(e))
    }, data)
};

export const register = (email, password) => {
    return (dispatch) => {
        authenticate({
            email: email,
            password: password,
            mode: 'register'
        }, dispatch)
    };
};

export const login = (email, password) => {
    return (dispatch) => {
        authenticate({
            email: email,
            password: password,
            mode: 'login'
        }, dispatch)
    };
};

export const authInit = () => {
    return {
        type: actionTypes.AUTH_INIT
    };
};


export const authSuccess = (data) => {
    const {token:token, ...user} = data;
    return {
        type: actionTypes.AUTH_SUCCESS,
        value: {
            token: token,
            user: user
        }
    };
};

export const authError = (data) => {
    return {
        type: actionTypes.AUTH_ERROR,
        value: data.errors
    };
};

export const authLogout = () => {
  return {
      type: actionTypes.AUTH_LOGOUT,
  }
};