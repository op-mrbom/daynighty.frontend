import * as actionTypes from '../actions/actionTypes';

const initialState = {
    searchInput: "",
    searchHints: [],
    searchTimeout: false,
    searchResultsLoading:false,
    searchResults: [],
    searchCount: null,
    showSearchOnMainScreen: true,
};

const showSearchOnMainScreen = (prevState, action) => {
    return {
        ...prevState,
        showSearchOnMainScreen: true,
        showSearchInNavbar: false,
    }
};

const showSearchInNavbar = (prevState, action) => {
    return {
        ...prevState,
        showSearchOnMainScreen: false,
        showSearchInNavbar: true,
    }
};

const searchSubmitInit = (prevState, action) => {
    return {
        ...prevState,
        searchResultsLoading: true
    }
};

const storeFetchedSearchResults = (prevState, action) => {
    return {
        ...prevState,
        searchResultsLoading: false,
        searchResults: action.value,
        searchCount: action.count,
        searchPage  : action.page
    }
};

const setHintsSearchTimeout = (prevState, action) => {
    const updated = {...prevState};
    return {
        ...updated,
        searchTimeout: action.value
    }

};

const resetHintsSearchTimeout = (prevState, action) => {
    return {
        ...prevState,
        searchTimeout: false,
    }
};

const searchChangeInput = (prevState, action) => {
    return {
        ...prevState,
        searchInput: action.value,
    }
};

const searchUpdateHints = (prevState, action) => {
    return {
        ...prevState,
        searchHints: action.value,
    }
};


const updateState = (prevState, action) => {
    const reactions = {
        [actionTypes.FAST_SEARCH_SET_TIMEOUT]: setHintsSearchTimeout,
        [actionTypes.FAST_SEARCH_RESET_TIMEOUT]: resetHintsSearchTimeout,
        [actionTypes.FAST_SEARCH_CHANGE_INPUT]: searchChangeInput,
        [actionTypes.FAST_SEARCH_UPDATE_HINTS]: searchUpdateHints,
        [actionTypes.SEARCH_SUBMIT_INIT]: searchSubmitInit,
        [actionTypes.SEARCH_RESULTS_FETCHED]: storeFetchedSearchResults,
        [actionTypes.SHOW_SEARCH_IN_NAVBAR]: showSearchInNavbar,
        [actionTypes.SHOW_SEARCH_ON_MAIN_SCREEN]: showSearchOnMainScreen
    };
    return reactions[action.type] !== undefined ? reactions[action.type](prevState, action) : prevState;
};


const reducer = (prevState = initialState, action) => {
    return updateState(prevState, action);
};

export default reducer;