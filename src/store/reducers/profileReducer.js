import * as actionTypes from '../actions/actionTypes';

const initialState = {
    profile: {},
    loading: false,
    errors: {},
    history: {
        loading: false,
        propositions: []
    }
};

const fetchInit = (prevState, action) => {
    return {
        ...prevState,
        loading: true
    }
};

const fetchErrors = (prevState, action) => {
    return {
        ...prevState,
        loading: false,
        errors: action.value
    }
};

const fetchSuccess = (prevState, action) => {
    return {
        ...prevState,
        loading: false,
        errors: {},
        profile: action.value
    }
};

const updateInit = (prevState, action) => {
    return {
        ...prevState,
        loading: true
    }
};

const updateErrors = (prevState, action) => {
    return {
        ...prevState,
        loading: false,
        errors: action.value
    }
};

const updateSuccess = (prevState, action) => {
    return {
        ...prevState,
        loading: false,
        errors: {},
        profile: action.value
    }
};


const fetchPropositionsBookHistorySuccess = (prevState, action) => {
    return {
        ...prevState,
        history: {
            propositions: action.value,
            loading: false,
        },
    }
};

const fetchPropositionsBookHistoryInit = (prevState, action) => {
    return {
        ...prevState,
        history: {
            ...prevState.history,
            loading: true,
        },
    }
};

const fetchPropositionsBookHistoryError = (prevState, action) => {
    return {
        ...prevState,
        history: {
            ...prevState.history,
            loading: false,
        },
    }
};

const updateState = (prevState, action) => {
    const reactions = {
        [actionTypes.PROFILE_FETCH_INIT]: fetchInit,
        [actionTypes.PROFILE_FETCH_SUCCESS]: fetchSuccess,
        [actionTypes.PROFILE_FETCH_ERROR]: fetchErrors,
        [actionTypes.UPDATE_PROFILE_INIT]: updateInit,
        [actionTypes.UPDATE_PROFILE_SUCCESS]: updateSuccess,
        [actionTypes.UPDATE_PROFILE_ERROR]: updateErrors,
        [actionTypes.FETCH_BOOK_HISTORY_INIT]: fetchPropositionsBookHistoryInit,
        [actionTypes.FETCH_BOOK_HISTORY_SUCCESS]: fetchPropositionsBookHistorySuccess,
        [actionTypes.FETCH_BOOK_HISTORY_ERROR]: fetchPropositionsBookHistoryError,
    };
    return reactions[action.type] !== undefined ? reactions[action.type](prevState, action) : prevState;
};

const reducer = (prevState = initialState, action) => {
    return updateState(prevState, action);
};

export default reducer;