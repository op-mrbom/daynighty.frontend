import * as actionTypes from '../actions/actionTypes';

const getOrSetToken = (token = null) => {
    if (token !== null)
        localStorage.setItem('jwtt', token);

    return localStorage.getItem('jwtt');
};

const getOrSetUser = (user = null) => {
    if (user !== null)
        localStorage.setItem('user', JSON.stringify(user));

    return localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {};
};

const removeToken = () => {
    localStorage.removeItem('jwtt');
};

const removeUser = () => {
    localStorage.removeItem('user');
};

const initToken = getOrSetToken();

const initialState = {
    user: getOrSetUser(),
    authenticated: !!initToken,
    token: initToken,
    loading: false,
    errors: {}
};

const authInit = (prevState, action) => {
    return {
        ...prevState,
        loading: true
    }
};

const authLogout = (prevState, action) => {
    removeToken();
    removeUser();
    return {
        ...prevState,
        user: {},
        token: null,
        authenticated: false
    }
};

const authErrors = (prevState, action) => {
    removeToken();
    removeUser();
    return {
        ...prevState,
        token: false,
        authenticated: false,
        loading: false,
        user: null,
        errors: action.value
    }
};

const authSuccess = (prevState, action) => {
    const token = getOrSetToken(action.value.token);
    const user = getOrSetUser(action.value.user);
    return {
        ...prevState,
        loading: false,
        user: user,
        token: token,
        authenticated: token !== null
    }
};

const authFromBook = (prevState, action) => {
    const token = getOrSetToken(action.value.user.token);
    const user = getOrSetUser(action.value.user);
    return {
        ...prevState,
        user: user,
        token: token,
        authenticated: token !== null
    }
};

const updateState = (prevState, action) => {
    const reactions = {
        [actionTypes.AUTH_INIT]: authInit,
        [actionTypes.AUTH_SUCCESS]: authSuccess,
        [actionTypes.AUTH_ERROR]: authErrors,
        [actionTypes.AUTH_LOGOUT]: authLogout,
        [actionTypes.BOOK_PROPOSITION_SUCCESS]: authFromBook
    };
    return reactions[action.type] !== undefined ? reactions[action.type](prevState, action) : prevState;
};

const reducer = (prevState = initialState, action) => {
    return updateState(prevState, action);
};

export default reducer;