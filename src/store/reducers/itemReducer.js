import * as actionTypes from '../actions/actionTypes';

const initialState = {
    itemData: {},
    itemLoading: true,
};

const itemFetchInit = (prevState, action) => {
    return {
        ...prevState,
        itemLoading: true
    }
};

const itemFetchSuccess = (prevState, action) => {
    return {
        ...prevState,
        itemData: action.value,
        itemLoading: false
    }
};

const updateState = (prevState, action) => {
    const reactions = {
        [actionTypes.ITEM_FETCH_INIT]: itemFetchInit,
        [actionTypes.ITEM_FETCH_SUCCESS]: itemFetchSuccess,
    };
    return reactions[action.type] !== undefined ? reactions[action.type](prevState, action) : prevState;
};

const reducer = (prevState = initialState, action) => {
    return updateState(prevState, action);
};

export default reducer;