import * as actionTypes from '../actions/actionTypes';

const initialState = {
    isAvailable: null,
    checkout: null,
    information: {},
    checkAvailabilityLoading: false,
    book: {
        loading: false,
        isBooked: null,
        byNewUser: null,
    },
};

const availabilityCheckSuccess = (prevState, action) => {
    return {
        ...prevState,
        checkAvailabilityLoading: false,
        isAvailable: action.value.isAvailable,
        information: action.value.information
    }
};

const availabilityCheckInit = (prevState, action) => {
    return {
        ...prevState,
        checkAvailabilityLoading: true,
    }
};

const availabilityCheckError = (prevState, action) => {
    return {
        ...prevState,
        checkAvailabilityLoading: false
    }
};

const setPropositionToCheckout = (prevState, action) => {
    return {
        ...prevState,
        checkout: action.value
    }
};


const bookSuccess = (prevState, action) => {
    return {
        ...prevState,
        book: {
            isBooked: true,
            loading: false,
            byNewUser: action.value.byNewUser
        },
    }
};

const bookInit = (prevState, action) => {
    return {
        ...prevState,
        book: {
            ...prevState.book,
            loading: true,
        },
    }
};

const bookError = (prevState, action) => {
    return {
        ...prevState,
        book: {
            loading: false,
        },
    }
};

const resetPropositionCheckout = (prevState, action) => {
    return initialState;
};

const updateState = (prevState, action) => {
    const reactions = {
        [actionTypes.CHECK_PROPOSITION_AVAILABILITY_SUCCESS]: availabilityCheckSuccess,
        [actionTypes.CHECK_PROPOSITION_AVAILABILITY_INIT]: availabilityCheckInit,
        [actionTypes.CHECK_PROPOSITION_AVAILABILITY_ERROR]: availabilityCheckError,
        [actionTypes.SET_PROPOSITION_TO_CHECKOUT]: setPropositionToCheckout,
        [actionTypes.BOOK_PROPOSITION_INIT]: bookInit,
        [actionTypes.BOOK_PROPOSITION_SUCCESS]: bookSuccess,
        [actionTypes.BOOK_PROPOSITION_ERROR]: bookError,
        [actionTypes.RESET_PROPOSITION_CHECKOUT]: resetPropositionCheckout,
    };
    return reactions[action.type] !== undefined ? reactions[action.type](prevState, action) : prevState;
};

const reducer = (prevState = initialState, action) => {
    return updateState(prevState, action);
};

export default reducer;