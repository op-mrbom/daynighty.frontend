import { combineReducers } from 'redux';

import searchReducer from './searchReducer';
import filterReducer from './filterReducer';
import itemReducer from './itemReducer';
import authReducer from './authReducer';
import profileReducer from './profileReducer';
import propositionReducer from './propositionReducer';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    search: searchReducer,
    searchFilter: filterReducer,
    item: itemReducer,
    auth: authReducer,
    profile: profileReducer,
    proposition: propositionReducer,
    form: formReducer
});