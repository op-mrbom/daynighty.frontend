import React from 'react';
import {NavLink} from 'react-router-dom';
import classes from './NavLink.less';

const navLink = (props) => {
  return (
      <NavLink
          className = {classes["navLink"]}
          to = {props.to}
          exact = {props.exact}
          onClick={props.onClick}
          activeClassName = {props.activeClassName !== undefined ? props.activeClassName : classes["navLink--active"]}>
          {props.children}
      </NavLink>
  );
};

export default navLink;