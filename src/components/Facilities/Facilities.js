import React from 'react';
import Facility from './Facility';
import E from '../../hoc/EmptyWrapper/EmptyWrapper';
import classes from './Facilities.less';

const facilities = (props) => {
    return (
        <E>
            <h4 className={classes["facilities__title"]}> Зручності: </h4>
            <div className={classes["facilities__list"]}>
                {props.facilities.map((v, i) => <Facility key={v.id} {...v}/>)}
            </div>
        </E>
    );
};

export default facilities;