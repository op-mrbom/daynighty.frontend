import React from 'react';
import classes from './Facilities.less';
import Image from '../UI/Image/Image';

export default (props) => {
    let icon = <div className={[classes["facility__icon"], classes["facility__icon--empty"]].join(' ')}></div>;
    if(props.icon)
        icon = <Image
                url={props.icon}
                className={classes["facility__icon"]}
                backgroundRepeat={'no-repeat'}
                positionX={'center'}
                positionY={'center'}
                width={32}
                height={32}
                backgroundSize={'contain'}/> ;

    return (
        <div className={classes.facility}>
            <span>
                {icon}
            </span>
            <span className={classes['facility__text']}>{props.nameUk}</span>
        </div>
    )
};