import React from 'react';
import Item from './SearchItem';
import classes from './SearchItems.less';

const searchItemsList = (props) => {

    let content = props.items.map((v, i) => {
        return (
            <Item
                onItemClick = {props.onItemClick}
                id={v.id}
                descriptionTitle={v.title}
                descriptionText={v.text}
                descriptionPrice={v.price}
                descriptionAddress={v.location}
                descriptionImage={v.img}
                key={v.id || i}
            />
        );
    });

    if(content.length === 0)
        content = (<h2>Упс... <br/> Ми не знайшли жодної пропозиції для Вас ...</h2>);

    return (

        <div className={classes["search-items"]}>
            {content}
        </div>
    );
};

export default searchItemsList;