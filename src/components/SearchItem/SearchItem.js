import React from 'react';
import Image from '../UI/Image/Image';
import classes from './SearchItems.less';

const searchItem = (props) => {
    const renderIf = (toRender, val, renderOtherwise) => !!val ? toRender : renderOtherwise || null;

    return (
        <div className={classes["item"]}>
            <Image
                onClick={() => props.onItemClick(props.id)}
                className={classes["item__image"]}
                url={props["descriptionImage"]}
                backgroundRepeat={'no-repeat'}
                positionX={'center'}
                positionY={'center'}
                backgroundSize={'cover'}
                />
            <div className={classes["description"]}>
                <div
                    onClick={() => props.onItemClick(props.id)}
                    className={classes["description__title"]}>
                    {props["descriptionTitle"]}
                </div>
                {
                    renderIf((
                        <div className={classes["description__address"]}>
                            {props["descriptionAddress"]}
                        </div>), props["descriptionAddress"])
                }
                <div className={classes["description__description"]}>
                    {props["descriptionText"]}
                </div>
                {
                    renderIf((
                        <div className={classes["description__price"]}>
                            {`Від ${props["descriptionPrice"]}грн`}
                        </div>
                    ), props["descriptionPrice"], (
                        <div className={classes["description__price"]}>
                            {"Ой... ціна недоступна"}
                        </div>
                    ))
                }
            </div>
        </div>
    );
};

export default searchItem;