import React from 'react';
import classes from './profileNav.less'

const nav = ({buttons}) => {
    const nButtons = buttons.map((v, i) => {
        v = {
            ...v,
            key: i,
            props: {
                ...v.props,
                activeClassName: classes['settings__item--active'],
                className: classes['settings__item']
            },
        };
        return v;
    });

    return (
        <div className={classes.settings}>
            {nButtons}
        </div>
    );
};

export default nav;