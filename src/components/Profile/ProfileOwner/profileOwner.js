import React, {Fragment} from 'react';
import Toggle from 'react-toggle';
import 'react-toggle/style.css';
import classes from './profileOwner.less'
import Button from '../../UI/Button/Button';


const profileOwner = (props) => {
    return (
        <div className={classes.owner}>
            <h2 className={classes['owner__header']}>
                Найважливіші речі спочатку. Нам необхідно знати, яким чином Ви будете використовувати наш сервіс. Ви будете надавати свою нерухомість в оренду або ж навпаки хочете бронювати житло?
            </h2>
            <div className={classes['owner__platform']}>
                <h5 className={classes['platform__propagation-text']}>
                    Будь ласка, оберіть варіант, що підходить Вам
                </h5>
                <label className={classes['platform__row']}>
                    <Toggle
                        checked={props.businessProfileChecked}
                        onChange={props.handleToggleChange}
                        name='business'
                        value={'business'}
                        />
                    <span className={classes['row__text']}>Я буду здавати житло в оренду</span>
                </label>
                <label className={classes['platform__row']}>
                    <Toggle
                        checked={props.regularProfileChecked}
                        onChange={props.handleToggleChange}
                        name='regular'
                        value={'regular'}
                    />
                    <span className={classes['row__text']}>Я буду бронювати житло</span>
                </label>
                <Button
                    additionalClasses={[classes['platform__row'], classes['row__button']]}
                    onClick={props.onContinueClicked}>
                    Продовжити
                </Button>
            </div>
        </div>
    );
};

export default profileOwner;