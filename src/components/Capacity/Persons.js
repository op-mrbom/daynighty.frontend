import React from 'react';
import classes from './Capacity.less';

const persons = (props) => {
    let classNames = [classes["persons"]];
    if(props.additionalClasses)
        classNames.push(...props.additionalClasses);

    return props.amount ? (
        <div className={classNames.join(' ')}>
            {`x${props.amount}`}
        </div>
    ) : null;
};

export default persons;