import React from 'react';
import classes from './Capacity.less';

const smallBeds = (props) => {
    let classNames = [classes["bed--small"]];
    if(props.additionalClasses)
        classNames.push(...props.additionalClasses);

    return props.amount ? (
        <div key={props.amount} className={classNames.join(' ')}>
            {`x${props.amount}`}
        </div>
    ) : null;
};

export default smallBeds;