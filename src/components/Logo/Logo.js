import React from 'react';
import classes from './Logo.less';
import {Link} from 'react-router-dom';

export default () => {
    return (
        <Link
            to="/">
            <div className={classes.logo}></div>
        </Link>
    );
}