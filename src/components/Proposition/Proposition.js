import React from 'react';

import Facilities from '../Facilities/Facilities';
import Book from '../../containers/Book/Book';

import Persons from '../Capacity/Persons';
import SmallBeds from '../Capacity/SmallBeds';
import LargeBeds from '../Capacity/LargeBeds';

import classes from './Proposition.less';

const proposition = React.forwardRef((props, ref) => {
    return (
        <div className={classes.proposition} ref={ref}>
            <h3 className={classes["proposition__title"]}>{props.name}</h3>
            <h3 className={[classes["proposition__info-item"], classes["proposition__header--small"]].join(' ')}>
                Доступно: {props.amountAvailable}</h3>
            <h3 className={[classes["proposition__info-item"], classes["proposition__header--small"]].join(' ')}>
                Вартість: {props.price}грн</h3>
            <div className={classes["proposition__main-info"]}>
                <Persons amount={props.capacity} additionalClasses={[classes["proposition__info-item"]]}/>
                <SmallBeds amount={props.smallBeds} additionalClasses={[classes["proposition__info-item"]]}/>
                <LargeBeds amount={props.largeBeds} additionalClasses={[classes["proposition__info-item"]]}/>
            </div>
            <p className={classes["proposition__description"]}>
                {props.information}
            </p>
            <Facilities facilities={props.facilities}/>
            <Book propositionId = {props.id} propositionName={props.name}/>
        </div>
    );
});

export default proposition;