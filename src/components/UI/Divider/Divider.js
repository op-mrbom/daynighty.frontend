import React from 'react';
import classes from './Divider.less';

export default () => {
    return <div className={classes.divider}></div>
}