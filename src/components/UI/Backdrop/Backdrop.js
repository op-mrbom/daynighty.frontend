import React from 'react';
import classes from './Backdrop.less';

export default (props) => {
    return (
        <div
            className={classes.backdrop}
            onClick={props.onClick}>
        </div>
    );
}