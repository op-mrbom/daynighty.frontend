import React from 'react';

const image = (props) => {
    const url = props.width && props.height
        ? `${props.url}?width=${props.width}&height=${props.height}`
        : props.url;

    const allowedProps = {...props};

    delete allowedProps.backgroundRepeat;
    delete allowedProps.positionX;
    delete allowedProps.positionY;
    delete allowedProps.backgroundSize;
    delete allowedProps.backgroundAttachment;
    delete allowedProps.url;
    delete allowedProps.width;
    delete allowedProps.height;

    return (<div
        {...allowedProps}
        className={props.className}
        style={{
            "background": `url(${url}) ${props.backgroundRepeat} ${props.positionX} ${props.positionY}`,
            "backgroundSize": `${props.backgroundSize}`,
            "backgroundAttachment": `${props.backgroundAttachment || 'unset'}`,
        }}>
    </div>);
};

export default image;