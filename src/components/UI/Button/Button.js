import React from 'react';
import classes from './Button.less';

export default (props) => {
    return (
        <button
            className={props.className ? props.className : [classes.btn, ...(props.additionalClasses || [])].join(' ')}
                onClick={props.onClick ? props.onClick : null}>
            {props.children}
        </button>
    );
};