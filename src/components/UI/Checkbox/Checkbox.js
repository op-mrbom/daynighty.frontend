import React from 'react';
import classes from './Checkbox.less';

const checkbox = (props) => {
    let checkClasses = [classes["checkbox-row__check"]];
    if(props.checked)
        checkClasses.push(classes["checkbox-row__check--checked"]);
    return (
        <div className={classes["checkbox-row"]}>
            <label
                className={classes["checkbox-row__label"]}>
                <input
                    type="checkbox"
                    name={props.name}
                    id={props.id || props.name.toLowerCase()}
                    className={classes["checkbox-row__input"]}
                    onChange={props.onChange}/>
                <span className={checkClasses.join(' ')}></span>
                {props.label}
            </label>
        </div>
    );
};

export default checkbox;