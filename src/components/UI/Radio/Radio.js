import React from 'react';
import classes from './Radio.less';

const radio = (props) => {
    let radioClasses = [classes["radio-row__radio"]];
    if(props.checked)
        radioClasses.push(classes["radio-row__radio--checked"]);
    return (
        <div className={classes["radio-row"]}>
            <input
                type="radio"
                name={props.name}
                id={props.id || props.name.toLowerCase()}
                className={classes["radio-row__input"]}
                onChange={props.onChange}/>
            <span className={radioClasses.join(' ')}></span>
            <label
                className={classes["radio-row__label"]}
                htmlFor={props.id || props.name.toLowerCase()}>
                {props.label}
            </label>
        </div>
    );
};

export default radio;