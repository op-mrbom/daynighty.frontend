import React from 'react';
import Button from '../UI/Button/Button';
import classes from './AvailablePlace.less';

import SmallBed from '../Capacity/SmallBeds';
import LargeBed from '../Capacity/LargeBeds';
import Persons from '../Capacity/Persons';

const availablePlace = (props) => {
    let beds = [];
    if(props.smallBeds)
        beds.push(<SmallBed
            key={props.smallBeds}
            amount={props.smallBeds}
            additionalClasses={[classes.bed]}/>);

    if(props.largeBeds)
        beds.push(<LargeBed
            key={props.largeBeds}
            amount={props.largeBeds}
            additionalClasses={[classes.bed]}/>);

    let wrapperClasses = [classes["available-place"]];
    if(props.isOpen)
        wrapperClasses.push(classes["available-place--active"]);
    return (
        <div className={wrapperClasses.join(' ')}>
            <p className={classes["available-place__name"]}>{props.name}</p>
            <Persons amount={props.capacity} additionalClasses={[classes["available-place__capacity"]]}/>
            <div className={classes["available-place__beds"]}>{beds}</div>
            <div className={classes["available-place__price"]}>{props.price}грн</div>
            <Button
                additionalClasses={[classes["available-place__button"]]}
                onClick={() => props.onShowDetailsClick(props.id)}>
                Подробиці
            </Button>
        </div>
    );
};

export default availablePlace;