import React from 'react';
import Button from '../UI/Button/Button';

export default (props) => {
    return (
        <Button className={props.cssClass}
                clicked={props.onClick ? props.onClick : null}>
            {props.children}
        </Button>
    );
};