import React from 'react';
import Btn from './SearchButton';

import classes from './Search.less';
import HintsList from './Hints/HintsList';

const searchBar = React.forwardRef((props, ref) => {
    const hints = props.searchHints && props.searchHints.length > 0 ? <HintsList values={props.searchHints}/> : null;

    return (
        <form
            className={props.searchWrapperClass ? props.searchWrapperClass : classes['search']}
            onSubmit={props.onSearchSubmit}>
            <p className={classes["search__agitation"]}>{props.text}</p>
            <input type='text'
                   ref={ref && ref}
                   value={props.inputValue}
                   onChange={props.onChange}
                   className={props.searchInputClass ? props.searchInputClass : classes['search__input']}/>
            <Btn cssClass={props.searchButtonClass}>
                Пошук
            </Btn>
            {hints}
        </form>
    );
});
export default searchBar;