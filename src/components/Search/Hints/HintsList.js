import React from 'react';
import classes from './Hints.less';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions/searchActions';

import {withRouter} from 'react-router-dom';

const hintsList = (props) => {
    let hints = props.values.map((v, i) => {
        return (
            <p
                key={i}
                className={classes['hints__item']}
                onClick={props.onClick(v, props.history)}>
                {v}
            </p>
        );
    });

    hints = hints.slice(0,4);

    return (
        <div className={classes['hints']}>
            {hints}
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (searchValue, history) => () => dispatch(actions.searchSubmit(searchValue, history))
    }
};

export default connect(null, mapDispatchToProps)(withRouter(hintsList));