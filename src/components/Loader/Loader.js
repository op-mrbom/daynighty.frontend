import React from 'react';
import classes from './Loader.less';

export default () => {
    return (
        <div className={classes["sk-folding-cube"]}>
            <div className={[classes["sk-cube"], classes["sk-cube-1"]].join(' ')}></div>
            <div className={[classes["sk-cube"], classes["sk-cube-2"]].join(' ')}></div>
            <div className={[classes["sk-cube"], classes["sk-cube-4"]].join(' ')}></div>
            <div className={[classes["sk-cube"], classes["sk-cube-3"]].join(' ')}></div>
        </div>
    )
}