import React, {Component} from 'react';

import Nav from '../../components/Profile/Navigation/profileNav';
import ProfileOwner from '../../components/Profile/ProfileOwner/profileOwner';
import History from './History';
import Details from './Details';

import Container from '../../hoc/Container/Container';
import Loader from '../../components/Loader/Loader';
import {connect} from 'react-redux';
import {withRouter, Redirect, NavLink, Route, Switch} from 'react-router-dom';

import * as actions from '../../store/actions/profileActions';

import classes from './Profile.less';

class Profile extends Component {
    state = {
        profileTarget:{
            regular: false,
            business: false
        }
    };

    handleProfileTargetChange = (e) => {
        const newVals = {
            regular: e.target.name === 'regular' && e.target.checked,
            business: e.target.name === 'business' && e.target.checked,
        };

        this.setState({profileTarget: newVals});
    };

    onProfileTargetChosen = () => {
        const type = this.state.profileTarget.regular ? 'regular' : 'business';
        this.props.updateProfile(this.props.userId, {
            profileTarget: type,
            type: type
        })
    };

    onProfileDetailsSubmit = (values) => {
        this.props.updateProfile(this.props.userId, {
            ...values,
            profileTarget: this.props.data.type
        });

        return false;
    };

    componentDidMount() {
        this.props.fetchProfile(this.props.userId);
    }

    render() {
        let content = <Loader/>;

        if (!this.props.loading && this.props.data) {
            let buttons = [<NavLink key={'navToDetails'} to={`${this.props.match.url}/details`}>{"Подробиці профілю"}</NavLink>];

            const allPages = {
                details: <Route
                    key={'detailsPage'}
                    path={`${this.props.match.url}/details`}
                    render={() => <Details
                        mode={this.props.data.type}
                        onSubmit={this.onProfileDetailsSubmit}
                        initialValues={{...this.props.data}}/>}/>,
                chooseTarget: <Route key={'chooseTargetPage'} path={`${this.props.match.url}/business`}
                                     render={() => <ProfileOwner
                                         businessProfileChecked={this.state.profileTarget.business}
                                         regularProfileChecked={this.state.profileTarget.regular}
                                         handleToggleChange={this.handleProfileTargetChange}
                                         onContinueClicked={this.onProfileTargetChosen}
                                     />}
                />,
                admin: <Route key={'adminPage'} path={`${this.props.match.url}/item-admin`} render={() => <p>item-admin</p>}/>,
                history: <Route key={'historyPage'} path={`${this.props.match.url}/history`} component={History}/>
            };

            let pages = [allPages.details];
            let homePage = <Redirect from={`/profile`} to={`/profile/history`}/>;

            if (!this.props.data.type) {
                pages.push(allPages.chooseTarget);

                homePage = <Redirect from={`/profile`} to={`/profile/business`}/>;
                buttons.push(<NavLink key={'navToChooseTarget'} to={`${this.props.match.url}/business`}>{"Хочете стати нашим партнером?"}</NavLink>);

            } else if (this.props.data.type === 'regular') {
                pages.push(allPages.history);
                buttons.push(<NavLink key={'navToHistory'} to={`${this.props.match.url}/history`}>{"Історія бронювань"}</NavLink>);

            } else if (this.props.data.type === 'business') {
                pages.push(allPages.admin);
                buttons.push(<NavLink key={'navToAdmin'} to={`${this.props.match.url}/item-admin`}>{"Менеджмент"}</NavLink>);
                homePage = <Redirect from={`/profile`} to={`/profile/item-admin`}/>
            }
            content = (
                <Container>
                    <div className={classes.profile}>
                        <div className={classes['profile__left']}>
                            <Nav
                                buttons={buttons}/>
                        </div>
                        <div className={classes['profile__right']}>
                            <Switch>
                                {pages}
                                {homePage}
                            </Switch>
                        </div>
                    </div>
                </Container>
            );
        }

        return content;
    }
}

const mapStateToProps = (rState) => {
    return {
        loading: rState.profile.loading,
        data: rState.profile.profile
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProfile: (id) => dispatch(actions.fetchProfile(id)),
        updateProfile: (id, data) => dispatch(actions.updateProfile(id, data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Profile));