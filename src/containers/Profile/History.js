import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TextField from '@material-ui/core/TextField';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Paper from '@material-ui/core/Paper';
import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

import Loader from '../../components/Loader/Loader';

import * as actions from '../../store/actions/profileActions';

import classes from './Profile.less';
import branch from "../../hoc/Branch/branch";
import moment from 'moment';

export class History extends Component {
    componentDidMount() {
        this.props.fetchHistory(this.props.userId);
    }

    renderBookDetails = (book, proposition) => {
        let summary = (
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                {`${proposition.item.name} - ${proposition.name}`}
            </ExpansionPanelSummary>
        );

        let details = [
            <ListItem key={1} button><ListItemText primary="Назва місця" secondary={proposition.item.name} /></ListItem>,
            <ListItem key={2} button><ListItemText primary="Назва номеру" secondary={proposition.name} /></ListItem>,
            <ListItem key={3} button><ListItemText primary="Місто" secondary={proposition.item.cityName} /></ListItem>,
            <ListItem key={4} button><ListItemText primary="Дата заїзду" secondary={moment(book.dateFrom).format("DD.MM.YYYY")} /></ListItem>,
            <ListItem key={5} button><ListItemText primary="Дата виїзду" secondary={moment(book.dateTo).format("DD.MM.YYYY")} /></ListItem>,
            <ListItem key={6} button><ListItemText primary="Ціна" secondary={`${book.price} грн`} /></ListItem>,

        ];
        return (
            <ExpansionPanel key={proposition.id}>
                {summary}
                <ExpansionPanelDetails>
                    <List>
                        {details}
                    </List>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    };

    render() {
        return branch(this.props.loading, <Loader/>, (
            <div className={classes.history}>
                <h2 className={classes["profile__header"]}>Історія бронювань</h2>
                <Paper className={classes["history__table"]}>
                    {
                        this.props.propositions.map((v, i) => {
                            return this.renderBookDetails(v, v.proposition);
                        })
                    }
                </Paper>

            </div>
        ))
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchHistory: (id) => dispatch(actions.fetchHistory(id))
    }
};

const mapStateToProps = (rState) => {
    return {
        loading: rState.profile.history.loading,
        propositions: rState.profile.history.propositions,
        userId: rState.auth.user.id
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(History);