import React, {Component} from 'react';
import {Field, reduxForm, formValueSelector} from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from '@material-ui/core/Paper';
import {
    AutoComplete,
    Checkbox,
    DatePicker,
    TimePicker,
    RadioButtonGroup,
    SelectField,
    Slider,
    TextField,
    Toggle
} from 'redux-form-material-ui'

import classes from './Profile.less';

const generateFieldName = (mode) => (desiredName) => mode === 'regular' ? desiredName : `company${capitalize(desiredName)}`;
const capitalize = (word) => word.charAt(0).toUpperCase() + word.substring(1);


class Details extends Component {

    render() {
        let fields = [];

        if (this.props.mode === 'regular') {
            fields = [
                <Field key={'name'} name={generateFieldName(this.props.mode)('name')} component={TextField}
                       floatingLabelText={capitalize(generateFieldName(this.props.mode)('Ім\'я'))} fullWidth/>,
                <Field key={'surname'} name={generateFieldName(this.props.mode)('surname')} component={TextField}
                       floatingLabelText={capitalize(generateFieldName(this.props.mode)('Прівище'))} fullWidth/>,
                <Field key={'phone'} name={generateFieldName(this.props.mode)('phone')} component={TextField}
                       floatingLabelText={capitalize(generateFieldName(this.props.mode)('Номер телефону'))} fullWidth/>,
                <Field key={'email'} name={generateFieldName(this.props.mode)('email')} component={TextField}
                       floatingLabelText={capitalize(generateFieldName(this.props.mode)('Електронна пошта'))} fullWidth/>,
            ];
        } else {
            fields = [
                <Field key={'name'} name={generateFieldName(this.props.mode)('name')} component={TextField}
                       floatingLabelText={capitalize('Назва')} fullWidth/>,
                <Field type="number" key={'foundationYear'} name={generateFieldName(this.props.mode)('foundationYear')}
                       component={TextField} floatingLabelText={capitalize('Рік заснування')} fullWidth/>,
                <Field key={'phone'} name={generateFieldName(this.props.mode)('phone')} component={TextField}
                       floatingLabelText={capitalize('Контактний номер телефону')} fullWidth/>,
                <Field key={'email'} name={generateFieldName(this.props.mode)('email')} component={TextField}
                       floatingLabelText={capitalize('Контактна адреса електронної пошти')} fullWidth/>,
            ];
        }

        return (
            <form onSubmit={this.props.handleSubmit(this.props.onSubmit)} className={classes['details__form']}>
                <h2 className={classes['profile__header']}>Будь ласка, заповніть (або відредагуйте) інформацію про себе</h2>
                <Paper className={classes['form__fields-wrapper']}>
                    {fields}
                </Paper>
                <RaisedButton type="submit" disabled={this.props.pristine || this.props.submitting}>
                    Зберегти
                </RaisedButton>
            </form>
        );
    }
}

Details = reduxForm({
    form: 'profileDetailsForm',
})(Details);

export default Details;