import React, {Component} from 'react';
import Search from '../../components/Search/Search';
import classes from './MainScreen.less';
import {connect} from 'react-redux';
import * as actions from '../../store/actions/searchActions';

class MainScreen extends Component{

    state = {
        searchIntervalIsRunning: false
    };

    onSearchHandler = (e) => {
        this.props.onSearchInput(e.target.value);

        if(this.state.searchIntervalIsRunning === false) {
            let currentValue = e.target.value;
            const interval = setInterval(() => {
                if(!this.searchInputRef.current) {
                    clearInterval(interval);
                    this.setState({searchIntervalIsRunning: false});
                    return;
                }

                if (this.searchInputRef.current.value === currentValue) {
                    clearInterval(interval);
                    this.props.onSearchHints(this.searchInputRef.current.value);
                    this.setState({searchIntervalIsRunning: false});
                }

                currentValue = this.searchInputRef.current.value;
            }, 250);

            this.setState({searchIntervalIsRunning: interval});
        }
    };

    onSearchSubmitHandler = (e) => {
        e.preventDefault();
        this.props.onSearchSubmit(this.props.searchValue, this.props.history);
    };

    componentDidMount(){
        this.props.onComponentMount();
        this.searchInputRef = React.createRef();
    }

    componentWillUnmount(){
        clearInterval(this.state.searchIntervalIsRunning);
    }

    render(){
        return(
            <section
                className={classes['main-screen']}>
                <div className={classes['main-screen__background']}></div>
                <Search searchWrapperClass={classes['main-screen__search-bar']}
                        searchInputClass={classes['main-screen__search-input']}
                        searchButtonClass={classes['main-screen__search-button']}
                        onChange={this.onSearchHandler}
                        onSearchSubmit={this.onSearchSubmitHandler}
                        inputValue={this.props.searchValue}
                        searchHints={this.props.searchHints}
                        ref = {this.searchInputRef}
                        text="Подорожуєте та шукаєте готель або апартаменти?"/>
            </section>
        );
    }
}


const mapStateToProps = (rState) => {
    return {
        searchValue: rState.search.searchInput,
        searchHints: rState.search.searchHints,
        searchTimeout: rState.search.searchTimeout
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSearchHints: (value) => dispatch(actions.searchHints(value)),
        onSearchInput: (value) => dispatch(actions.updateSearchInput(value)),
        onSearchSubmit: (searchValue, history) => dispatch(actions.searchSubmit(searchValue, history)),
        onComponentMount: () => dispatch(actions.showSearchOnMainScreen())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);