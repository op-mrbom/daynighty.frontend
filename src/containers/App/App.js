import React, {Component} from 'react';
import Layout from '../../hoc/Layout/Layout';
import MainScreen from '../MainScreen/MainScreen';
import AuthForm from '../AuthForm/AuthForm';
import Item from '../Item/Item';
import Profile from '../Profile/Profile';
import SearchResult from '../SearchResult/SearchResult';
import Checkout from '../Proposition/PropositionCheckout';
import withAuth from '../../hoc/Auth/withAuth';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route
                        path="/search"
                        component={SearchResult}
                    />
                    <Route
                        path="/login"
                        render={props => {
                            return (<AuthForm showSignIn={true} {...props} />);
                        }}/>
                    }/>
                    <Route
                        path="/sign-up"
                        render={props => {
                            return (<AuthForm showSignUp={true} {...props} />);
                        }}/>
                    <Route
                        path="/item/:id"
                        render={props => {
                            return (<Item id={props.match.params.id} {...props}/>)
                        }}
                    />
                    <Route
                        path="/profile"
                        component={withAuth(Profile)}
                    />
                    <Route
                        path="/checkout"
                        component={Checkout}
                    />
                    <Redirect from='/home' to='/'/>
                    <Route
                        path="/"
                        exact
                        component={MainScreen}/>

                </Switch>
            </Layout>
        </BrowserRouter>
    );
};


export default App;