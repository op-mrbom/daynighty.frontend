import React, {Component, PureComponent} from 'react';
import {Route, withRouter} from 'react-router-dom';
import Container from '../../hoc/Container/Container';
import Divider from '../../components/UI/Divider/Divider';
import Proposition from '../../components/Proposition/Proposition';
import AvailablePlace from '../../components/AvailablePlace/AvailablePlace';
import Loader from '../../components/Loader/Loader';
import Image from '../../components/UI/Image/Image';
import branch from '../../hoc/Branch/branch';
import {connect} from 'react-redux';
import classes from './Item.less';
import * as actions from '../../store/actions/itemActions';

class Item extends PureComponent {

    state = {
        openProposition: false,
    };

    showPropositionDetails = (propId) => {
        this.setState({
            openProposition: propId
        })
    };

    componentDidMount() {
        this.props.fetchItem(this.props.match.params.id);
        this.openPropositionRef = React.createRef();
        window.scrollTo(0,0);
    }

    componentDidUpdate() {
        this.openPropositionRef.current && this.openPropositionRef.current.scrollIntoView();
    }

    render() {
        let openProposition = this.state.openProposition &&
            <Proposition {...this.props.data.propositions.find((v) => v.id === this.state.openProposition)} ref={this.openPropositionRef}/>;

        let content = <Loader/>;
        if(!this.props.loading)
            content = (
                <div className={classes["item"]}>
                    <Image
                        url={this.props.data.image}
                        className={classes["item__image"]}
                        backgroundSize={'cover'}
                        backgroundRepeat={'no-repeat'}
                        positionX={'center'}
                        positionY={'center'}
                        backgroundAttachment={'fixed'}/>

                    <div className={classes["item__core-information"]}>
                        <p className={classes["core__type"]}>{this.props.data.type.type}</p>
                        {
                            branch(this.props.data.address, (
                                <p className={classes["core__location"]}>{`${this.props.data.city.name}, ${this.props.data.address}`}</p>
                            ),(<p className={classes["core__location"]}>{this.props.data.city.name}</p>)
                            )
                        }
                        <h2 className={classes["core__title"]}>{this.props.data.name}</h2>
                        <p className={classes["core__description"]}>{this.props.data.information}</p>
                        <Divider/>
                        <div className={classes["available-places"]}>
                            {
                                this.props.data.propositions.map((v, i) => {
                                    return <AvailablePlace
                                            {...v} key={v.id}
                                            isOpen={this.state.openProposition === v.id}
                                            onShowDetailsClick={this.showPropositionDetails}/>
                                })
                            }
                        </div>
                        <Divider/>
                        {openProposition}
                    </div>
                </div>
            );

        return (
            <Container>
                {content}
            </Container>
        );
    }
}


const mapStateToProps = (rState) => {
    return {
        loading: rState.item.itemLoading,
        data: rState.item.itemData
    }

};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchItem: (id) => dispatch(actions.fetchItem(id))
    }

};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Item));