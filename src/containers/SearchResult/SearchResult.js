import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../store/actions/searchActions';
import Loader from '../../components/Loader/Loader';
import Filter from '../SearchFilter/SearchFilter';
import ItemsList from '../../components/SearchItem/SearchItemsList';
import Container from '../../hoc/Container/Container';
import Pagination from 'material-ui-pagination';
import {withRouter} from 'react-router-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import classes from './SearchResult.less';


class SearchResult extends Component {
    state = {
        currentPage: 1,
        pageSize: 10,
        totalCount: null
    };

    componentDidMount() {
        this.props.onComponentMount();
    }

    componentDidUpdate() {
        window.scrollTo(0, 0);
    }

    onPageChange = (newPage) => {
        this.props.fetchResultsPage(newPage, this.state.pageSize, this.props.searchValue);
    };

    onItemClick = (itemId) => {
        this.props.history.push(`/item/${itemId}`);
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        let toReturn = {};
        if (prevState.totalCount !== nextProps.totalCount)
            toReturn['totalCount'] = nextProps.totalCount;
        if (nextProps.currentPage !== prevState.currentPage)
            toReturn['currentPage'] = nextProps.currentPage;

        return toReturn || null;
    }

    render() {
        let content = <Loader/>;

        const itemsList = this.props.items.map((v, i) => {
            let location = v.cityName;
            if (v.address)
                location += `, ${v.address}`;

            return {
                ...v,
                img: v.image,
                text: v.description.substring(0, 200) + '...',
                title: v.name,
                location: location
            }
        });

        if (!this.props.loading) {
            let pagination = null;
            if (this.state.totalCount > this.state.pageSize)
                pagination = <Pagination
                    total={Math.ceil(this.state.totalCount / this.state.pageSize)}
                    current={parseInt(this.state.currentPage)}
                    display={10}
                    onChange={this.onPageChange}/>;

            content = (

                <MuiThemeProvider>
                    <Fragment>
                        <div className={classes["search-results"]}>
                            <Filter/>
                            <ItemsList
                                onItemClick={this.onItemClick}
                                //temporary
                                items={itemsList}/>
                        </div>
                        <div className={classes["search-results__pagination"]}>
                            {pagination}
                        </div>
                    </Fragment>
                </MuiThemeProvider>
            );
        }

        return (
            <Container>
                {content}
            </Container>
        );
    }
}

const mapStateToProps = (rState) => {
    return {
        loading: rState.search.searchResultsLoading,
        items: rState.search.searchResults,
        totalCount: parseInt(rState.search.searchCount),
        currentPage: parseInt(rState.search.searchPage),
        searchValue: rState.search.searchInput
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onComponentMount: () => dispatch(actions.showSearchInNavbar()),
        fetchResultsPage: (page, pageSize, value) => dispatch(actions.searchSubmit(value, null, page, pageSize))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchResult));