import React, {Component} from 'react';
import Loader from '../../components/Loader/Loader';

import classes from './AuthForm.less';

const signForm = (props) => {
    const createErrorMessages = (error) => {
        return error.map((v, i) => <span key={i} className={classes["sign-form__input-error"]}>{v}</span>)
    };

    let content = (
        <form className={classes["sign-form"]} onSubmit={(e) => {props.onSubmit(e); e.preventDefault()}}>
            <p className={classes["sign-form__text"]}>{props.mode === 'sign-in' ? 'Вхід' : 'Реєстрація'}</p>

            <div className={classes["sign-form__input-wrapper"]}>
                <span className={classes["sign-form__input-label"]}>Пошта</span>
                <input
                    type="text"
                    className={classes["sign-form__login-input"]}
                    onChange={props.onEmailChange}
                    value={props.emailValue}
                    name="email"/>
                {props.errors.email ? createErrorMessages(props.errors.email) : null}
            </div>
            <div className={classes["sign-form__input-wrapper"]}>
                <span className={classes["sign-form__input-label"]}>Пароль</span>
                <input
                    type="password"
                    className={classes["sign-form__password-input"]}
                    value={props.passwordValue}
                    onChange={props.onPasswordChange}
                    name="password"/>
                {props.errors.password ? createErrorMessages(props.errors.password) : null}

            </div>

            <button className={classes["sign-form__sign-button"]}>{props.mode === 'sign-in' ? 'Ввійти' : 'Зареєструватися'}</button>

            <div className={classes["sign-form__loader"]}>
                {props.loading ? <Loader/> : null}
            </div>
        </form>
    );

    if(!props.showForm){
        content = (
            <div className={classes.agitation}>
                <p className={classes["agitation__text"]}>{props.mode === 'sign-in' ? 'Вже маєте акаунт?' : 'Хочете створити акаунт?'} </p>
                <button
                    className={classes["agitation__button"]}
                    onClick={props.changeScreenClicked}>{props.mode === 'sign-in' ? 'Вхід' : 'Реєстрація'}!</button>
            </div>
        );
    }

    return content;
};

export default signForm;
