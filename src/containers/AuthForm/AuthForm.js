import React, {Component} from 'react';
import Container from '../../hoc/Container/Container';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import classes from './AuthForm.less';
import SignForm from './SignForm';

import * as actions from '../../store/actions/authActions';

class AuthForm extends Component{
    state = {
        showSignIn: this.props.showSignIn,
        showSignUp: this.props.showSignUp,
        errors: {
            email: null,
            password: null
        },
        email: "",
        password: ""
    };

    static getDerivedStateFromProps(nextProps, prevState){
        const r = {
            showSignIn: nextProps.showSignIn,
            showSignUp: nextProps.showSignUp
        };
        if (nextProps.formErrors && !nextProps.loading)
            r['errors'] = nextProps.formErrors;

        return r;
    }

    onFormChangeHandler = (e) => {
      if(this.state.showSignIn)
          this.props.history.push("/sign-up");
      else
          this.props.history.push("/login");
    };

    onInputChangeHandler = (e) => {
        const myE = {
            name: e.target.name,
            value: e.target.value
        };

        this.setState((prevState) => {
            return {
                [myE.name]: myE.value,
                errors: {
                    ...prevState.errors,
                    [myE.name]: null
                }
            }
        });
    };

    onRegisterFormSubmit = () => {
        this.props.register(this.state.email, this.state.password);
    };

    onLoginFormSubmit = () => {
        this.props.login(this.state.email, this.state.password);
    };

    render(){
        let signInClasses = [classes["auth-form__sign-in"]];
        let signUpClasses = [classes["auth-form__sign-up"]];

        if(this.state.showSignIn)
            signInClasses.push(classes["auth-form__sign-in--active"]);
        if(this.state.showSignUp)
            signUpClasses.push(classes["auth-form__sign-up--active"]);

        return (
            <Container centerContent={true}>
                {this.props.authenticated && <Redirect to={'/profile'}/>}
                <div className={classes["auth-form"]}>
                    <div className={signInClasses.join(' ')}>
                        <SignForm
                            showForm={this.state.showSignIn}
                            loading={this.props.loading}
                            mode="sign-in"
                            errors={this.state.errors}
                            onPasswordChange={this.onInputChangeHandler}
                            onEmailChange={this.onInputChangeHandler}
                            passwordValue={this.state.password}
                            emailValue={this.state.email}
                            onSubmit={this.onLoginFormSubmit}
                            changeScreenClicked={this.onFormChangeHandler}/>
                    </div>
                    <div className={signUpClasses.join(' ')}>
                        <SignForm
                            loading={this.props.loading}
                            showForm={this.state.showSignUp}
                            onPasswordChange={this.onInputChangeHandler}
                            onEmailChange={this.onInputChangeHandler}
                            passwordValue={this.state.password}
                            emailValue={this.state.email}
                            onSubmit={this.onRegisterFormSubmit}
                            mode="sign-up"
                            errors={this.state.errors}
                            changeScreenClicked={this.onFormChangeHandler}/>
                    </div>
                </div>
            </Container>
        );
    }
}

const mapStateToProps = (rState) => {
    return {
        loading: rState.auth.loading,
        formErrors: rState.auth.errors,
        authenticated: rState.auth.authenticated
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (email, password) => dispatch(actions.login(email, password)),
        register: (email, password) => dispatch(actions.register(email, password)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthForm);