import React, {PureComponent, Component, Fragment} from 'react';
import {SkyLightStateless} from 'react-skylight';
import 'react-dates/initialize';
import {DateRangePicker, SingleDatePicker, DayPickerRangeController} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import * as constants from 'react-dates/constants';
import * as actions from '../../store/actions/propositionActions';
import TextField from '@material-ui/core/TextField'
import branch from '../../hoc/Branch/branch';
import {connect} from 'react-redux';
import Button from '../../components/UI/Button/Button';
import Loader from '../../components/Loader/Loader';
import {withRouter} from 'react-router-dom';

import './Book.css';
import classes from './Book0.less';

import moment from "moment";

class Book extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate: null,
            focusedInput: null,
            showPicker: false,
            forceDataRefresh: false,
            amountOfPersons: 1,
            isAvailable: null
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.showPicker && !prevState.forceDataRefresh)
            return {isAvailable: nextProps.isAvailable};

        return null;
    }

    onCheckPriceClickHandler = () => {
        this.setState({
            showPicker: true
        });
    };

    onCheckPriceCloseClickHandler = () => {
        this.setState({
            showPicker: false,
            forceDataRefresh: true,
            isAvailable: null,
        });
    };

    onAmountOfPersonsChangeHandler = (v) => {
        this.setState({
            amountOfPersons: v
        });
    };

    onCheckAvailabilityClickHandler = () => {
        this.props.checkAvailability(this.state.startDate.format('X'), this.state.endDate.format('X'), this.state.amountOfPersons, this.props.propositionId);
        this.setState({forceDataRefresh: false});
    };

    onProceedToCheckoutHandler = () => {

        this.props.proceedToCheckout({
            id: this.props.propositionId,
            name: this.props.propositionName,
            from: this.state.startDate,
            to: this.state.endDate,
            price: this.props.bookInformation.price
        });

        this.props.history.replace('/checkout');
    };

    render() {
        let popupContent = null;
        if (this.state.showPicker)
            popupContent = branch(this.props.availabilityIsLoading, <Loader/>,
                branch(this.state.isAvailable !== null,
                    branch(this.state.isAvailable, (
                        <div className={classes["book__available-proposition"]}>
                            <h2 className={classes["book__header"]}>{`${this.props.propositionName} можливо забронювати на період ${this.state.startDate && this.state.startDate.format("DD.MM.YYYY")} - ${this.state.endDate && this.state.endDate.format("DD.MM.YYYY")} ${this.props.bookInformation && this.props.bookInformation.propositionsRequired > 1 ? 'у кількості ' + this.props.bookInformation.propositionsRequired : ''} за ціною ${this.props.bookInformation && this.props.bookInformation.price}грн`}</h2>
                            <Button onClick={this.onProceedToCheckoutHandler}
                                    additionalClasses={classes["book__check-button"]}>Продовжити</Button>
                        </div>
                    ), (
                        <h2 className={classes["book__header"]}>{`На жаль, ${this.props.propositionName} неможливо забронювати на період ${this.state.startDate && this.state.startDate.format("DD.MM.YYYY")} - ${this.state.endDate && this.state.endDate.format("DD.MM.YYYY")}`}</h2>
                    )), (<Fragment>
                        <h2 className={classes['book__header']}>Будь ласка, оберіть необхідну кількість персон</h2>
                        <TextField
                            label="Кількість"
                            value={this.state.amountOfPersons}
                            onChange={(e) => this.onAmountOfPersonsChangeHandler(e.target.value)}
                            type="number"
                            required
                            className={classes['book__persons-amount']}
                        />

                        <h2 className={classes['book__header']}>Будь ласка, оберіть дату заїзду та виїзду</h2>
                        <DateRangePicker
                            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                            startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                            endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                            onDatesChange={({startDate, endDate}) => {
                                this.setState({
                                    startDate, endDate
                                })
                            }} // PropTypes.func.isRequired,
                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                            onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                            orientation={window.matchMedia('(max-width: 768px)').matches ? constants.VERTICAL_ORIENTATION : constants.HORIZONTAL_ORIENTATION}
                            startDatePlaceholderText="Дата заїзду"
                            endDatePlaceholderText="Дата виїзду"
                        />
                        <Button
                            additionalClasses={[classes['book__check-button']]}
                            onClick={this.onCheckAvailabilityClickHandler}>Продовжити</Button>
                    </Fragment>)
                )
            );
        return (
            <div className={classes.book}>
                <Button
                    onClick={this.onCheckPriceClickHandler}
                    additionalClasses={[classes['book__check-button']]}>Перевірити доступність</Button>
                <SkyLightStateless
                    onOverlayClicked={this.onCheckPriceCloseClickHandler}
                    hideOnOverlayClicked
                    className={classes["book__check-popup"]}
                    isVisible={this.state.showPicker}
                    onCloseClicked={this.onCheckPriceCloseClickHandler}>
                    <div className={classes["book__popup-content-wrapper"]}>
                        {popupContent}
                    </div>
                </SkyLightStateless>
            </div>
        );
    }
}

const mapStateToProps = (rState) => {
    return {
        isAvailable: rState.proposition.isAvailable,
        bookInformation: rState.proposition.information,
        availabilityIsLoading: rState.proposition.checkAvailabilityLoading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        checkAvailability: (from, to, personsAmount, id) => dispatch(actions.checkIfAvailable(id, from, to, personsAmount)),
        proceedToCheckout: (data) => dispatch(actions.setCheckoutProposition(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Book));