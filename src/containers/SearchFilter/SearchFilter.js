import React, {Component} from 'react';
import classes from './SearchFilter.less';
import Checkbox from '../../components/UI/Checkbox/Checkbox';
import Radio from '../../components/UI/Radio/Radio';

class SearchFilter extends Component {
    state = {
      checkboxes:{
          'check-0': false,
      }
    };

    onChange = (e) => {
        e.persist();
        this.setState((prevState) => {
            return {
                checkboxes: {
                    ...prevState.checkboxes,
                    [e.target.name]: e.target.checked
                }
            }
        });
    };

    render(){
        return (
            <div className={classes["search-filter"]}>
                <p className={classes["search-filter__agitation"]}>
                    search filter
                </p>
                <div className={classes["search-filter__filters"]}>
                    <Checkbox
                        label={"Check me please"}
                        name="check-0"
                        onChange={this.onChange}
                        checked={this.state.checkboxes['check-0']}/>
                </div>
            </div>
        );
    }
};

export default SearchFilter;