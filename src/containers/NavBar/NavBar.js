import React, {Component} from 'react';
import Logo from '../../components/Logo/Logo';
import NavLink from '../../components/NavBar/NavLink';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import SearchBar from '../../components/Search/Search';
import E from '../../hoc/EmptyWrapper/EmptyWrapper';

import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import * as actions from '../../store/actions/searchActions';
import * as authActions from '../../store/actions/authActions';

import classes from "./NavBar.less";

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showMobileMenu: false,
        };


        this.scrollStoredValue = null;
        this.navbarRef = React.createRef();
    }

    onMenuToggleClick = (e) => {
        this.setState((prevState, prevProps) => {
            return {
                showMobileMenu: !prevState.showMobileMenu
            }
        });
    };

    onBackdropClick = (e) => {
        this.setState({
            showMobileMenu: false,
        })
    };

    onSearchSubmit = (e) => {
        e.preventDefault();

        this.hideMobileMenu();
        this.props.onSearchSubmit(this.props.searchValue, this.props.history);
    };

    hideMobileMenu = () => {
        this.setState({
            showMobileMenu: false,
        });
    };

    onSearchInputChange = (e) => {
        this.props.onSearchInputChange(e.target.value);
    };

    onScroll = (e) => {
        if (this.scrollStoredValue === null) {
            this.scrollStoredValue = window.pageYOffset;
            return;
        }

        if (window.pageYOffset > this.scrollStoredValue && window.pageYOffset > this.navbarRef.current.clientHeight) {
            if (!this.navbarRef.current.classList.contains(classes['navbar--minified']))
                this.navbarRef.current.classList.add(classes['navbar--minified']);

            this.scrollStoredValue = window.pageYOffset;
        }

        else if (window.pageYOffset < this.scrollStoredValue && this.scrollStoredValue - window.pageYOffset > this.navbarRef.current.clientHeight) {
            this.navbarRef.current.classList.remove(classes['navbar--minified']);
            this.scrollStoredValue = window.pageYOffset;
        }

    };

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll)
    }

    render(){
        let navbarMenuClasses = [classes['navbar__right']];
        if(this.state.showMobileMenu)
            navbarMenuClasses.push(classes['navbar__right--mobile-opened']);

        let searchBar = null;
        if(this.props.withSearchBar){
            searchBar = (
                <SearchBar
                    onSearchSubmit={this.onSearchSubmit}
                    inputValue={this.props.searchValue}
                    onChange={this.onSearchInputChange}
                    searchWrapperClass={classes["navbar__search-bar"]}
                    searchInputClass={classes["search-bar__input"]}
                    searchButtonClass={classes["search-bar__button"]}/>
            );

        }

        let authButtons = (
          <E>
              <NavLink
                  to="/sign-up"
                  exact={true}
                  onClick={() => this.hideMobileMenu()}>
                  {"Реєстрація"}
              </NavLink>
              <NavLink
                  to="/login"
                  onClick={() => this.hideMobileMenu()}>
                  {"Ввійти"}
              </NavLink>
          </E>
        );
        if (this.props.isAuthenticated)
            authButtons = (
                <E>
                    <NavLink
                        to="/profile"
                        onClick={() => {
                            this.hideMobileMenu();
                        }}>
                        {"Профіль"}
                    </NavLink>
                    <NavLink
                        to="/home"
                        onClick={() => {
                            this.hideMobileMenu();
                            this.props.logout();
                        }}>
                        {"Вийти"}
                    </NavLink>
                </E>
            );

        return(
            <nav ref={this.navbarRef} className={classes.navbar}>
                <div className={classes['navbar__left']}>
                    <Logo/>
                </div>
                <div
                    className={classes['navbar__menu-toggle']}
                    onClick={this.onMenuToggleClick}>
                    <span className={classes['toggle__line']}></span>
                    <span className={classes['toggle__line']}></span>
                    <span className={classes['toggle__line']}></span>
                </div>
                <div
                    className={navbarMenuClasses.join(' ')}>
                    {searchBar}
                    {this.state.showMobileMenu && authButtons}
                </div>
                <div className={classes['navbar__right']}>
                    {authButtons}
                </div>
                {this.state.showMobileMenu
                    ? <Backdrop onClick={this.onBackdropClick}/>
                    : null}
            </nav>
        );
    }
}

const mapStateToProps = (rState) => {
    return {
        withSearchBar: rState.search.showSearchInNavbar,
        searchValue: rState.search.searchInput,
        isAuthenticated: rState.auth.authenticated
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(authActions.authLogout()),
        onSearchSubmit: (searchValue, history) => dispatch(actions.searchSubmit(searchValue, history)),
        onSearchInputChange: (value) => dispatch(actions.updateSearchInput(value))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavBar));