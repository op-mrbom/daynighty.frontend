import React, {Component, Fragment} from 'react';
import Container from '../../hoc/Container/Container';
import * as actions from '../../store/actions/propositionActions';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TextField from '@material-ui/core/TextField';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '../../components/UI/Button/Button';
import branch from '../../hoc/Branch/branch';
import Loader from '../../components/Loader/Loader';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import classes from './PropositionCheckout.less';

class PropositionCheckout extends Component {
    state = {
        form: {
            email: "",
            phone: "",
            errors: {
                email: false,
                phone: false
            }
        }
    };

    renderPropositionTableRows = () => {
        const keysToRender = {
            name: 'Назва номеру',
            price: 'Вартість номеру',
            description: 'Опис номеру',
            capacity: 'Місткість номеру',
        };
        let res = [];

        for (let i in this.props.proposition) {
            if (i in keysToRender) {
                res.push((
                    <TableRow key={`proposition-${i}`}>
                        <TableCell className={classes["proposition-table__cell--header"]}>{keysToRender[i]}</TableCell>
                        <TableCell
                            className={classes["proposition-table__cell"]}>{this.props.proposition[i]}</TableCell>
                    </TableRow>
                ));
            }
        }

        return res;
    };

    renderItemTableRows = () => {
        const keysToRender = {
            name: 'Назва місця',
            cityName: 'Місто'
        };
        let res = [];

        for (let i in this.props.item) {
            if (i in keysToRender) {
                res.push((
                    <TableRow key={`item-${i}`}>
                        <TableCell className={classes["proposition-table__cell--header"]}>{keysToRender[i]}</TableCell>
                        <TableCell className={classes["proposition-table__cell"]}>{this.props.item[i]}</TableCell>
                    </TableRow>
                ));
            }
        }

        return res;
    };

    onCheckoutClickHandler = () => {
        let errors = {};
        if (this.state.form.email.length === 0 && !this.props.token)
            errors.email = true;

        if (this.state.form.phone.length === 0 && !this.props.token)
            errors.phone = true;

        if (Object.keys(errors).length !== 0) {
            this.setState((prevState) => {
                return {
                    form: {
                        ...prevState.form,
                        errors: errors
                    }
                }
            });
        } else {
            this.props.bookProposition({
                propositionId: this.props.proposition.id,
                authToken: this.props.token,
                email: this.state.form.email,
                phone: this.state.form.phone,
                from: this.props.bookInformation.from,
                to: this.props.bookInformation.to
            });
        }
    };

    onInputChangeHandler = (e) => {
        e.persist();
        this.setState((prevState) => {
            return {
                form: {
                    ...prevState.form,
                    [e.target.name]: e.target.value
                }
            }
        })
    };

    componentWillUnmount() {
        this.props.resetCheckout();
    }

    render() {
        return (
            <Container>
                {branch(this.props.isCheckout,
                    branch(this.props.bookLoading, <Loader/>,
                        branch(this.props.checkoutSuccess, (
                                <div className={classes.checkout}>
                                    <h2 className={classes['checkout__header']}>Бронювання успішно!</h2>
                                    {branch(this.props.isUserNew, (<h2 className={classes['checkout__header']}>Для Вас було створено аккаунт. Детальну інформацію про логін та пароль було надіслано на {this.props.userEmail} </h2>))}
                                    <h2 className={classes['checkout__header']}>У Вашому профілю з'явилася інформація про заброньований номер</h2>
                                    <Button
                                        onClick={() =>  this.props.history.replace('/profile')}
                                        additionalClasses={[classes["checkout__button"]]}>Перейти у профіль</Button>
                                </div>
                            ), (<div className={classes.checkout}>
                                    <h2 className={classes['checkout__header']}>Ви намагаєтесь забронювати наступну
                                        пропозицію:</h2>
                                    <Paper className={classes['checkout__proposition-table']}>
                                        <Table>
                                            <TableBody>
                                                {this.renderItemTableRows()}
                                                {this.renderPropositionTableRows()}
                                            </TableBody>
                                        </Table>

                                    </Paper>
                                {
                                    branch(!this.props.token, (
                                        <Fragment>
                                            <h2 className={classes['checkout__header']}>Щоб мати можливість це зробити, необхідно
                                                заповнити
                                                наступні поля</h2>
                                            <Paper className={classes['checkout__form']}>
                                                <TextField
                                                    error={this.state.form.errors.email}
                                                    fullWidth
                                                    key={'email'}
                                                    name={'email'}
                                                    type={'email'}
                                                    onChange={this.onInputChangeHandler}
                                                    label={'Електронна пошта'}/>
                                                <TextField
                                                    error={this.state.form.errors.phone}
                                                    fullWidth
                                                    onChange={this.onInputChangeHandler}
                                                    key={'phone'}
                                                    name={'phone'}
                                                    type={'text'}
                                                    label={'Номер телефону'}/>
                                            </Paper>
                                        </Fragment>
                                    ))
                                }

                                <Button
                                    additionalClasses={[classes["checkout__button"]]}
                                    onClick={this.onCheckoutClickHandler}>Забронювати</Button>
                                </div>
                            )
                        )
                    ), <div className={classes.checkout}><h2 className={classes["checkout__header"]}>Упс... щось пішло не так</h2></div>)
                }
            </Container>
        );
    }
};

const mapStateToProps = (rState) => {
    return {
        isCheckout: rState.proposition.checkout,
        token: rState.auth.token,
        userId: rState.auth.user.id,
        userEmail: rState.auth.user.email,
        isUserNew: rState.proposition.book.byNewUser,
        proposition: rState.proposition.information.proposition,
        item: rState.proposition.information.item,
        bookInformation: rState.proposition.information,
        checkoutSuccess: rState.proposition.book.isBooked,
        bookLoading: rState.proposition.book.loading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        bookProposition:({
                             propositionId,
                             from,
                             to,
                             email,
                             phone,
                             authToken
        }) => dispatch(actions.bookProposition({
            id: propositionId,
            from: from,
            to: to,
            email: email,
            phone: phone,
            authToken: authToken || null
        })),
        resetCheckout: () => dispatch(actions.resetCheckout())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PropositionCheckout));