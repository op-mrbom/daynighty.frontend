import React from 'react';
import {SkyLightStateless} from 'react-skylight';
import E from '../EmptyWrapper/EmptyWrapper';
import {withRouter} from 'react-router-dom';

import {setAuthToken, removeAuthToken, setErrorHandler, removeErrorHandler} from '../../api/apiRequests';

import {connect} from 'react-redux';
import * as actions from '../../store/actions/authActions';

const withAuth = (WrappedComponent) => {
    class C extends React.Component {
        constructor (props){
            super (props);
            setAuthToken(props.authToken);
            setErrorHandler(this.onAuthError);

            this.state = {
                showPopup: !this.props.isAuthenticated
            }
        }

        componentWilUnmount() {
            removeErrorHandler();
            removeAuthToken();
        }

        onModalClose = () => {
            this.setState({showPopup: false}, this.onAfterModalClose);
        };

        onAfterModalClose = () => {
            this.props.history.replace('/login')
        };

        onAuthError = (e) => {
            if (e.status && e.status === 401) {
                this.props.logout();
                this.setState({
                    showPopup: true
                });
            }
        };

        render () {
            const modal = (<SkyLightStateless
                                hideOnOverlayClicked
                                title={"You need to be authorized to perform this action"}
                                isVisible={this.state.showPopup}
                                onCloseClicked={this.onModalClose}
                                onOverlayClicked={this.onModalClose}
                                transitionDuration={200}>
                                {"Please authorize"}
                            </SkyLightStateless>);
            return (
                <E>
                    {modal}
                    {this.props.isAuthenticated
                        ? <WrappedComponent {...this.props}/>
                        : modal}
                </E>

            );
        }
    }



    const mapStateToProps = (rState) => {
        return {
            isAuthenticated: rState.auth.authenticated,
            authToken: rState.auth.authenticated ? rState.auth.token : null,
            userId: rState.auth.authenticated ? rState.auth.user.id : null
        }
    };

    const mapDispatchToProps = (dispatch) => {
        return {
          logout: () => dispatch(actions.authLogout())
        };
    };

    return connect(mapStateToProps, mapDispatchToProps)(withRouter(C));
};

export default withAuth;