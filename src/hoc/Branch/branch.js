import React from 'react';

const branch = (condition, onSuccess, onFailure = null) => {
    return condition ? onSuccess : onFailure
};

export default branch;