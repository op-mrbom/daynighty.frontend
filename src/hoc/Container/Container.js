import React from 'react';
import classes from './Container.less';

const container = (props) => {
    let containerClasses = [classes.container];
    if(props.centerContent)
        containerClasses.push(classes["container--centered-content"]);
    return (
      <div className={containerClasses.join(' ')}>
          {props.children}
      </div>
    );
};

export default container;