import React, {Component} from 'react';
import EmptyWrapper from '../EmptyWrapper/EmptyWrapper';
import NavBar from '../../containers/NavBar/NavBar';
import {createMuiTheme} from '@material-ui/core';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#1f9040",
            light: "#1f9040",
            dark: "#1f9040",
            contrastText: "#1f9040",
        },
    }
});

class Layout extends Component {
    constructor(props) {
        super(props);

        this.props = props;
        this.state = {};
    }


    render() {
        return (
            <EmptyWrapper>
                <NavBar/>
                <MuiThemeProvider theme={theme}>
                    <main>
                        {this.props.children}
                    </main>

                </MuiThemeProvider>
            </EmptyWrapper>
        );
    }
}

export default Layout;
