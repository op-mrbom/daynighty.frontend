class RequestProcessor {
    requests = {};
    delays = {};

    static PROCESSING_IDLE = -1;
    static PROCESSING_ACTIVE = -2;
    static PROCESSING_STOPPED = -3;
    static PROCESSING_DELAYED = -4;

    processing = RequestProcessor.PROCESSING_IDLE;

    setRequest = (req, key) => {
        if (this.requests[key] === undefined)
            this.requests[key] = [];

        this.requests[key].push(req);
    };

    getRequest = (key) => {
        return this.requests[key] && this.requests[key].length > 0 ? this.requests[key].pop() : null;
    };

    shouldBeDelayed = (key) => {
        return this.delays[key] && this.requests[key] && this.delays[key] !== this.requests[key].length;
    };

    recordDelaySnapshot = (key) => {
        if(this.delays[key] === undefined)
            this.delays[key] = -1;

        this.delays[key] = this.requests[key].length;
    };

    processLastRequest = (key, delay) => {
        const run = () => {
            if(this.processing === RequestProcessor.PROCESSING_STOPPED) {
                this.processing = RequestProcessor.PROCESSING_IDLE;
                return;
            }

            this.processing = RequestProcessor.PROCESSING_ACTIVE;

            if(this.shouldBeDelayed(key))
                this.processing = RequestProcessor.PROCESSING_DELAYED;

            if(this.processing === RequestProcessor.PROCESSING_ACTIVE)
                setTimeout(processRequest, delay);
            else if(this.processing === RequestProcessor.PROCESSING_DELAYED)
                setTimeout(run, delay);

            this.recordDelaySnapshot(key);
        };
        const processRequest = () => {
            if (this.processing === RequestProcessor.PROCESSING_STOPPED || this.processing === RequestProcessor.PROCESSING_DELAYED)
                return;

            const req = this.getRequest(key);
            if (req !== null) {
                req();
                this.requests = {};
                this.processing = RequestProcessor.PROCESSING_STOPPED;
            }

        };

        run();

    }
}

export default RequestProcessor;