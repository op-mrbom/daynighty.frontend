import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://daynighty.abc/api/'
});

export default instance;