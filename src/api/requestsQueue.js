class Queue {

    requests = {};
    interval = {};

    INTERVAL_EMPTY = -1;
    INTERVAL_CANCELLED = -2;
    INTERVAL_RESUMED = -3;

    getRequest(key) {
        return this.requests[key].length > 0 ? this.requests[key].pop() : null;
    }

    setRequest(requestFunction, key) {
        if(this.requests[key] === undefined)
            this.requests[key] = [];

        this.requests[key].unshift(requestFunction);
    }

    cancelQueue = (key) => {
        clearInterval(this.interval[key]);
        this.interval[key] = this.INTERVAL_CANCELLED;
    };

    resumeQueue = (key) => {
        if(this.interval[key] === this.INTERVAL_CANCELLED)
            this.interval[key] = this.INTERVAL_RESUMED;
    };

    processQueue = (key, stepTime) => {
        if(!this.interval[key] || this.interval[key] === this.INTERVAL_RESUMED || (this.interval[key] === this.INTERVAL_EMPTY && this.interval[key] !== this.INTERVAL_CANCELLED)) {
            this.interval[key] = setInterval(() => {
                const r = this.getRequest(key);
                if(!r) {
                    clearInterval(this.interval[key]);
                    this.interval[key] = this.INTERVAL_EMPTY;
                    return;
                } else {
                  r();
                }

            }, stepTime)
        }
    }
}

export default new Queue();