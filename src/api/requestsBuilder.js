import axios from "./axiosInstance";

class RequestBuilder {
    errorHandlerAttached = false;
    authHeaderAttached = false;
    authToken = null;

    send = axios;

    build = (func, onSuccess, onError, raw = false) => {
        return async () => {
            try {
                const r = await func();
                onSuccess(raw ? r : r.data);
            } catch (e) {
                let errorData = null;
                if (e.response)
                    errorData = e.response.data;
                else if (e.request)
                    errorData = e.request;
                else
                    errorData = e.message;

                onError(errorData);
            }
        };
    };

    attachErrorHandler = (f) => {
        if (!this.errorHandlerAttached) {
            this.errorHandlerAttached = axios.interceptors.response.use((response) => {
                if (response.status !== 200)
                    f(response.data);

                return Promise.resolve(response);
            }, (error) => {
                if (error.response)
                    f(error.response.data);
                else if (error.request)
                    f(error.response.data);
                else
                    f(error.message);

                return Promise.reject(error);
            });
        }
    };

    detachErrorHandler = () => {
        if (this.errorHandlerAttached !== false) {
            axios.interceptors.response.eject(this.errorHandlerAttached);
            this.errorHandlerAttached = null;
        }
    };


    withAuthHeader = (token) => {
        this.authToken = token;
        if (!this.authHeaderAttached) {
            this.authHeaderAttached = axios.interceptors.request.use((config) => {
                config.headers = {
                    ...config.headers,
                    'Authorization': `Bearer ${this.authToken}`
                };
                return config;
            })
        }
    };

    detachAuthHeader = () => {
        if (this.authHeaderAttached !== false) {
            axios.interceptors.request.eject(this.authHeaderAttached);
            this.authHeaderAttached = false;
            this.authToken = null;
        }
    };

    addRelations = () => {
        axios.interceptors.request.use((config) => {
            if (config.params) {
                for (let k in config.params) {
                    if (k.substr(0, 12) === 'withRelation') {
                        config.params.expand = config.params.expand || [];
                        config.params.expand.push(k.substring(12).toLowerCase());
                        delete config.params[k];
                    }
                }

                if (config.params.expand)
                    config.params.expand = config.params.expand.join(',');
            }

            return config;
        });
    };

    addValidationRule = () => {
        axios.interceptors.request.use((config) => {
            config.validateStatus = (status) => status === 200;
            return config;
        });

    };
}

export default RequestBuilder;