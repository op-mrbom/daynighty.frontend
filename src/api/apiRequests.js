import RequestBuilder from './requestsBuilder';

const requestBuilder = new RequestBuilder ();
requestBuilder.addRelations();
requestBuilder.addValidationRule();

export function setAuthToken (token) {
    requestBuilder.withAuthHeader(token);
}

export function removeAuthToken () {
    requestBuilder.detachAuthHeader();
}

export function setErrorHandler (f) {
    requestBuilder.attachErrorHandler(f);
}

export function removeErrorHandler () {
    requestBuilder.detachErrorHandler();
}

export function performCitySearch(searchString, onSuccess, onError) {
    requestBuilder.build(async () => await requestBuilder.send.post('/city/search', {
        q: searchString,
        limit: 4,
    }), onSuccess, onError)();
}

export function performItemSearch(city, pageSize, page, onSuccess, onError) {
    requestBuilder.build(async () => {
        return await requestBuilder.send.post('/items/search', {
            city: city,
            pageSize: pageSize,
            page: page
        })
    }, onSuccess, onError, true)();
}

//as second argument options are passed
//this might contain a lot of stuff
export function fetchItem(id, onSuccess, onError, {
    withPropositions = false,
    withType = false,
    withLocation = false,
    withCity = false
}) {
    requestBuilder.build(async () => {
        return await requestBuilder.send.get(`/items/${id}`, {
            params: {
                withRelationPropositions: withPropositions,
                withRelationType: withType,
                withRelationLocation: withLocation,
                withRelationCity: withCity
            }
        })
    }, onSuccess, onError)();
}

export function checkIfPropositionAvailable(id, from, to, personsAmount, onSuccess, onError) {

    requestBuilder.build(async () => {
        return await requestBuilder.send.get(`/propositions/${id}/check-availability`, {
            params: {
                from: from,
                to: to,
                personsAmount: personsAmount
            }
        })
    }, onSuccess, onError)();
}

export function performPropositionBook({id, authToken, from, to, email, phone}, onSuccess, onError) {
    requestBuilder.build(async () => {
        return await requestBuilder.send.post(`/propositions/${id}/book`,  {
            from: from,
            to: to,
            email: email,
            phone: phone,
            authToken: authToken
        })
    }, onSuccess, onError)();
}

//AUTH

export function performAuthenticate(onSuccess, onError, {email, password, mode}) {
    const method = mode === 'register' ? 'register' : 'login';
    requestBuilder.build(async () => {
        return await requestBuilder.send.post(`/auth/${method}`, {
            email: email,
            password: password
        })
    }, onSuccess, onError)();
}

//PROFILE

export function performProfileFetch(onSuccess, onError, userId) {
    requestBuilder.build(async () => {
        return await requestBuilder.send.get(`/profiles/${userId}`, {})
    }, onSuccess, onError)();
}

export function performProfileUpdate(onSuccess, onError, userId, data) {
    requestBuilder.build(async () => {
        return await requestBuilder.send.put(`/profiles/${userId}`, data)
    }, onSuccess, onError)();
}

export function performBookHistoryFetch(userId, onSuccess, onError) {
    const rel = "withRelationProposition.item";
    requestBuilder.build(async () => {
        return await requestBuilder.send.get(`/propositions/history/${userId}`, {
            params: {
                [rel]: 1
            }
        })
    }, onSuccess, onError)();
}